<!-- TITLE: Inventory -->
<!-- SUBTITLE: Ravenshade's Inventory and Property -->

# Joy Bag of Holding
-----

## Essential
* 1140p 627g 1028s 513c

## Equipment
* 1 x dwarven longsword
* 1 x leather armor
* 1 x plate armor (One w/ BigToe)
* 2 x long bow
* 1 x shortsword
* 2 x Cult of Sin robes
* 1 x Cult of Sin robe (halfling)
* 1 x Green Vampire Robe
* 0 x Lightning Javelin (Two with Wulskin)
* 1 x Gauntlets of Ogre Strength (Alonzo)
* 10 x Philsopher Stones
* Thumadoin's Cloak
* Thumadoin's Wand - +2 wand, cursed
* +2 Scimitar
* +1 Breastplate armor
* +1 Rapier
* Robes of Magi - 12 AC + dex (attunement)
* Rod of Magi - +2 Spell Attack (attunement)
* 2 x Keoghtom's Ointment
* 1 x Colt Pistol
* 20 x Colt Pistol Ammo
* 9 x Grenades (Joy)
* 1 x Brass Flute
* Kiss of the Changebringer (neck, defiance, all savings throws +2, immune restrain grapple, blink (1 long))
* Nightblade (+2 shortsword, 2d6 slash + 1d4 psychic) (Joy) 
* Cynthia's Harp -- Ollamh's Harp
* Lion's Courage Amulet (adv saving throw frighten)
* Cloak of Invisibility (Joy)

## Beasts
* 6 Milderwood Horses (Riding Horse +10m)
* 1 Kyle the Camel (now a god)
* 1 Kylo the Camel

## Potions
* 0 x Healing Potion
* Potion of Greater Healing

## Miscellaneous
* 3 x wooden cartwheels
* Blue Metal Crystal Magical Key
* 1 x canvas scraps 
* 1 x barrel of alcohol?
* 1 x cannon ball
* 7 x tents
* 6 x traveler's clothes
* 1 x captain's hat
* 1 x rings
* 3 x gems horde
* 1 x Wind Fan
* Letters from Soren to Meira
* 1 x Black Opal (1000g)
* 300 lb Magiquartz

## Tools and Kits
* 1 x carpenter's tools
* 1 x healer's kit
* 1 x climber's kit
* 2 x shovels

## Books
* 0 x Dirty Porn Book
* 1 x Feydrin Book: **Mirror Murderer** 10,000g summon, 5000g for levels of power
* Thumadoin's Locked Spellbook - philosopher stone
* Animal-skin Manual - requires leatherworking
* Legend of the Ancient Dog
* Bible of Loth 
* Zinzerena (magical - necromancy, illusion)

## Quest Items
* 0 x Difficult to Spell Items
* 1 x Zolia's Body, 1 x Crow's Body

## Contraptions
### 1 x Mechanical Pigeon
* Anywhere malestare - couple days
* Terracloven - 1 week

# Property
## Udoven
* [The Ravenshade Estate](/places/udoven_kingdom/ravenshade_estate)

## Drovania
 * [Fidget's Forest](/places/fidgets-forest)
## Yltrona
* [Collhen Keep](/places/collhen-keep)
## Ship - Kord's Blessing
* (Sea Ship) [Kord's Blessing](/ships/kords_blessing)
* (Air Ship) Skyrunner - 2 Alrich Cannons, ~40ft long

## Venture
* [Greenbottle Apothecary](/shops/greenbottle-apothecary)
* [Raven's Nook](/shops/ravens-nook)
* [Raven's Nest](/places/ravens-nest)