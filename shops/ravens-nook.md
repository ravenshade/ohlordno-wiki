<!-- TITLE: Raven's Nook -->
<!-- SUBTITLE: Ravenshade's Magical Item & Curiosities Shop -->

# Raven's Nook
* Owned: Fidget & Tobin (50/50)
* Located in Republic of Udoven
* Purchased Lot for 4000 G

# Employees
* Esmeralda - 40g / mo
* Shadow the Raven
* Lily the Raven

# Inventory
## Curiosities
* 12 x Hermit Crab - Free w/ Purchase
* 20 x Lavendar Candle - 1g
* 15 x Black Runic Chalk - 10g
* 1 x Sovereignty Inn Gold Key Card "14B" - 15g
* 4 x Fancy Robes - 20g
* 1 x Necklace (400g) - 400g

## Magical
* 5 x Everburning Candles - 105g
* 1 x Staff of Flowers - 500g
* 1 x Mariner's Armor - 1,000g
* 1 x +2 Warhammer - 5,000g
* 1 x Magical Book of Medicine - 12,000g
* 1 x Canaith Mandolin - 14,000g
* 1 x Fire Mage Spellbook - 15,000g
# Style
* Sign: Hanging Sign, shape of crest, purple & gold, black border, gold serif lettering & raven
