<!-- TITLE: Joy Quickfoot -->
<!-- SUBTITLE: A quick summary of Joy Quickfoot -->

# Hi I'm Joy :D
https://twitter.com/JQuickfoot


# Equipt
1. Ice dagger
2. Flame dagger
3. Black Renewal
* Darkvision goggles
* Boots of Elven Kind
* Manta Ray Cloak
* Polar Pike
* Basic Potion of Healing
* Studded leather armor
* 2 x grenades
* Paralyzing Poison
# Inventory
Bag of Holding
* Terra Cloven ID
* Light blue dress
* Lock
* 10 x Everburning candles
* 8 x fancy candles (assorted scents lavender, rose, etc.)
* Cut opal bracelet
* 15 x daggers
* 10 x rations
* Platnum Medal for winning Victory Pit
* White Sequin Glove
* Paint brushes
* Black paint
* Glow-in-the-dark paint

# Quests
## TODO
* Kill fake members of the [Nightstalkers](/orgs/nightstalkers)

[Coast City](/places/coastcity)
* Members of Spice Guild ?
* [Count Chris Romanoff](/chars/count_chris_romanoff), son wants him dead so he can inherit everything

[Mountain of Secrets](/places/Mountain_of_Secrets)
* Clownkiller ?
* Embarass [Seethe](/char/Seethe)

## Complete
[City of Ames](/places/City_of_Ames)
* Kill Baker [Bryce Dewit](/chars/Bryce_Dewit)

[Terra Cloven](/places/Terra_Cloven)
* Get signature from [King Haorel](/chars/Haorel_Willokin) to release 3rd finger of the Nightstalkers from the prison of the abyss 
* Kill [Jason Straith](/chars/Jason_Straith)
* Kill the [Black Razor](/chars/Black_Razor)

## Backlog
Udoven
* There was a dragonborn general who wanted someone to try and knock him out as a challenge in his elder years. (deceased in Udoven coup appearently) 

# Relations
* Mary Quickfoot (mother)