<!-- TITLE: Inventory -->
<!-- SUBTITLE: A quick summary of Leobin's Inventory -->

# Inventory
## Precious Items
* Fresh Cut Large Sapphire (1000 gp)
* Cut opal stone 
* magic quartz bracelet (Opal fits in bracelet)
* Finger nail puller thing
* Gold Terracloven id card
* Platinum Harel Victory Pit Metal
* 1, 10 lbs magi quartz chunks (white)
* 8, 1 pound magi quartz chunks (white)
* 1, 10 lbs magic quartz (red)
* 1, 20 lbs magic quartz chunk white
* Father’s signature name plate from clockwork warden
* Portable Hole

## Weapons
* Frozen Will (AKA Adam’s sword): d8+2d4cold (hit twice move half)
* Compound Magic Bow (+1 to hit)
* 1 bear traps
* 4 Hunting traps
* 2 Revolvers
* 1 Bad Ass Alrich Canon
* Dragon Slayer (longsword, 1d8 Slashing + str)(Attunement, not attuned)

## Armor, Clothing, & Apparel (Equipped) 
* Studded Leather Armor
* Necklace of Nondetection (attunement)
* Animated Cape 
* Head Band of Intellect (Sets intellect to 19 once attuned)
* Plague Doctor Mask 
* Pistol holsters
* Elvin Chain Mail (no attunement, +1 to AC)
* Hiking Pack (Large)
* Quiver of Elona (Bag of holding for ammunition)

## Armor, Clothing, & Apparel (Un-equipped) 
* Black (Plauge Dr.) Cloak 
* Bracers of Archery (+2 to bow hit, attunement)

## Ammunition
* 60 bullets
* 249 normal arrows
* 20 silver arrows
* 10 +2 ammunition (arrow type)
* 3 Alchemist Fire Arrows
* 0 Explosive arrows (1 d8 + 2 D6 explosive + 4 pierce) (bonus action to light)
* 0 Magic arrow (luck arrow)
* 3 grappling arrows
* 0 Acid arrows (D8+4 piercing+ 2 D6 Acid)
* 0 Black Magic Arrow of Human Slaying (DC Con save 17, 6D10 piercing or half if saved)

## Tools
* Tinker's Tools
* Carpenter's Tools
* Woodcarver’s Tools
* Smith’s Tools

## General Supplies 
* 25 torches 
* 30 ft of rope
* Pick axe
* 2 enimat bags
* 1 pouch of drugs 
	•	3 crystalized drake root
	•	1 red eye
	•	Bag of dried battla fuit
	•	1 yellow tounge

## Maps
* World Map
* MT. of Secrets Map
* Map of ocean (includes island of glass)

## Books
* "Tusk Love” Book	
* Book of Prayer for paylor 
* How to make acid for dummies book
* King Gelgar’s Dataless puzzle diary 
* Bible of Loth
