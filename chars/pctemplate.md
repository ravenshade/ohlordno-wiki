<!-- TITLE: PC Name -->
<!-- SUBTITLE: Quick summary of class and specialization -->

# PC Name
 * [Inventory](/chars/pctemplate/inventory)
 * Born DATE
 * Height: HEIGHT
 * Voice: VOICE

## Skills
* 

## Languages
* 

## Tools
* 

# Achievements
## Accomplishments
* 
## Titles
* 


# Quests
* 

# Factions

## Relations

#### [FACTION](/orgs/FACTION)
Brief Description
# History

## Backstory
* 