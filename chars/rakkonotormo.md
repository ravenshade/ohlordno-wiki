<!-- TITLE: Rakkon Otormo -->
<!-- SUBTITLE: Goliath Battle Bard (Valor) -->
# Rakkon Otormo
 * [Inventory](/chars/rakkon_otormo/inventory)
 * Born 1982
 * Height: 7'10"
 * Voice: Soft thunder
## Skills
Jack of all trades, master of...
* Athletics
* History
* Perception
* Performance
* Persuasion
* Survival
## Languages
* Common
* Dwarvish
* Giant
## Tools
* War Horn
* Harp
* Lyre
* Mandolin
# Achievements
## Accomplishments
* Surviving
## Titles
* Lorebearer of the Otormos
# Quests
* 
# Factions
## Relations
#### [Ravenshade](/orgs/ravenshade)
Rakkon is a member of Ravenshade.
# History
## Backstory
* 