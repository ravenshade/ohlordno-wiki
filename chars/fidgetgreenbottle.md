<!-- TITLE: Fidget Greenbottle -->
<!-- SUBTITLE: A Lightfoot Hobbit Moon Druid -->

# Fidget Greenbottle
 * [Inventory](/chars/fidget_greenbottle/inventory)
 * Born 1994
 * Height: 2.8 ft
 * Voice: British voice, inspired by Bilbo Baggins
 * Has Raven on Hindquarters in form

## Skills
* Perception
* Survival
* Animal Handling
* Stealth

## Languages
* Druidic
* Common
* Halfling

## Tools
* Cook's Utensils
* Herbalism Kit

# Achievements
## Accomplishments
* Peed on Demon King

## Titles
* Grand Chef of Udoven
* Ravenshade Initiate
* Ravenshade Member


# Quests
* Free [Bertha the Bear](/chars/Bertha_the_bear) from the prison [Island of Betrayers](/places/leonins_forest/island_of_betrayers)
* Learn as many animal forms as possible
* "Beware the Lord of the Hunt, He is coming."
* Create a Democracy/Republic **done**
* Woo Joy **done**
* Gain Proficiency in Alchemy **done**
* Become a Master Chef **done**
* Destroy a Tyrannical Government **done**

# Property
* [Raven's Nook](/shops/ravens-nook)
* [Greenbottle Apothecary](/shops/greenbottle-apothecary)

# Factions

## Relations

#### [Ravenshade](/orgs/ravenshade)
Fidget is a member of Ravenshade.

#### [Udoven Kingdom](/orgs/udoven_kingdom)
Fidget is the Grand Chef of Udoven. 

#### [Nightstalkers](/orgs/nightstalkers)

# History

## Backstory
* Born in 1994 to [Sam](/chars/sam_greenbottle) and [Nora Greenbottle](/chars/nora_greenbottle)
* Honey badger, chased by uncle hunter into leonin's forest
* Saved by Fafine the flying eagle.
* Leonin kicked me out when realized not animal, but halfling
* Beforehand befriended Bertha the Bear (trapped)
* Pretended to be the pet of Joy's family
* Took a week before they figured out I wasn't a dog.
* Met up with them.
* Stole a golden egg for them