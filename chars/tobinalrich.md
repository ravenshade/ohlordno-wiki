<!-- TITLE: Leon (Tobin) Alrich -->
<!-- SUBTITLE: A quick summary of Tobin Alrich -->

 * [Inventory](/chars/tobinalrich/inventory)
 * Born: ?
 * Height: 6.4 ft
 * Voice: Formerly gruff, mild Scottish accent for the hill dwarf days; Now a mild British accent for the wood elf days


## Skills
* Perception
* Survival
* Investigation
* Stealth

## Languages
* Dwarvish
* Common
* Elvish
* Silvin

## Tools
* Tinker's Tools
* Carpenter's Tools
* Woodcarver’s Tools
* Smith’s Tools

# Achievements
## Accomplishments
* Killed King Adam
* Killed the young red drgaon [Kyrasyl](/chars/kyrasyl) (rolled a 10, she was super hawt)
* Invented a exploding arrow
* Invented a mechanical grappling arrow
* Invented an acid arrow
* Won in the Terracloven Victory Pit; Schooled everyone in a preceeding drinking contest 
* Made 1st construct: A clockwork beetle 
* Killed [Jason Straithe](/chars/jason_straithe) twice, using his feet and bound hands to shoot a bow from a moving elevator 
* Killed a young blue dragon
* Returned from the dead as a wood elf

## Titles
* Ravenshade Founder: Last of the Forming Members


# Quests
* Go into the feywild to find more leads on Leo
* Aldun asked for Tobin to bring Driewith back to him. Driewith is Tobin's gnomish brother, last seen in the City of Thieves 
* ~~Develop first clockwork construct ~~
* Find more magiquartz
# Factions

## Relations

#### [Ravenshade](/orgs/ravenshade)
Tobin is a member of Ravenshade.

#### [Udoven Kingdom](/orgs/udoven_kingdom)
Tobin is currently aligned with the rightful king of Udoven. No official position given. Just there for whatever happens.

#### [Alrich Automatons](/orgs/alrichautomatons)
The Alrich family business back in Terracloven. Tobin technically works for them, but is doing so remotly at the moment.
# History
## Backstory
Tobin was adopted by Aldun Aldrich in the city of Terracloven. Tobin's real parents are a mystery, and he was never told under what circumstances he came to end up at the orphenage in the outer district of the city. When he was still an infant, Aldun adopted the dwarf in order to build a family which he could one day choose a member from to take over the operation of [Alrich Automatons](/orgs/alrichautomatons). Tobin is the youngest of his adopted brothers and while he loves tinkering, he has always wanted to get away from the city. Going on adventures let him see new places and get away from some of his older brothers with whom he didn't get along with well. Tobin only would return to the city to purchase hard to find tinkering materials, and see his beloved younger sister Angelica. Tobin and Angelica are blood relatives and were adopted at the same time by Aldun. Tobin has always been close to his sister and they each looked out for one another while growing up. 

As Tobin was nearing adulthood, he began wanting to explore further outside the outer walls of Terracloven. This waunderlust is what ultimately drawn Tobin and his stange younger brother Leo together. Leo was the least interested of the brothers in the family business. Leo wanted to spend his time in nature, far from the clatter of the Alrich Workshops. Tobin found a mutual bond with Leo that him the only brother Tobin truely cared for. His time with Leo made Tobin familiar with the natural world and made him also want to become a skilled hunter. Tobin decided he wanted to learn the ways of the ranger in order to protect himself and survive on the road. Meanwhile Leo was searching for his true place in life. There was nothing for Leo in Terracloven, and eventually he set out on a long trip to the eastern kingdoms to try to find himself. When Leo failed to return from a trip to the eastern kingdom of Udoven, Tobin was the only member of the family that seemed to care. The others viewed Leo as the blacksheep. A rather strange human that always seemed to have his head in the clouds. 

Tobin set off to try to find his borther and bring him back if possible. His search would take him through Coe City, and to the Udoven Port. From there he traveled all around trying to find any lead he could that would point him in the right direction. Eventually he began to run low on supplies and funds as he turned his search to the south. 