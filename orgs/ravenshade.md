<!-- TITLE: Ravenshade -->
<!-- SUBTITLE: A group of degenerates that attempts to be heroes -->

# Ravenshade
## General
 * [Party Inventory and Property](/orgs/ravenshade/inventory)


## Members
 * [Wulskin Crarrhaal](/chars/wulskin_crarrhaal)
 * [Fidget Greenbottle](/chars/fidget_greenbottle)
 * [Joy Quickfoot](/chars/joy_quickfoot)
 * [Rakkon Otormo](/chars/rakkon_otormo)
 * [Tobin Alrich](/chars/tobin_alrich)
 * [Ott](/chars/ott)

## Former Members
* [Dwyte Goodwin](/chars/dwyte_goodwin) (left)
* [Diana Holiday](/chars/diana_holiday) (left)
* [Xolia](/chars/xolia) (left)
* [Thrum Merringal](/chars/thrum_merringal) (left/unknown)
* [Porntip Mallagher](/chars/porntip_mallagher) (deceased)
* [Brox "Bladune" Badune](/chars/brox_badune) (deceased)
* [Merida Underwood](/chars/merida_underwood) (deceased)

# Achivements
## Titles
* Heroes of Udoven
* Champions of the Victory Pit
* Grand Champion of the Battle Royale

# Factions

## Allies
* [King Urdok](/chars/joseph_urdok) (deceased)
* [Queen Mira Alduin](/chars/mira_alduin)
* [Gelladrill](/chars/gelladrill)
* [Keladryl](/chars/keladryl)
* [Sam "Bloodyaxe" Malone](/chars/sam_malone)
* [Graves](/chars/graves)
* [Clovis](/chars/clovis)
* [Atticus Finch](/chars/atticus_finch)
* [Rita](/chars/rita)
* [Harvey Resort](/chars/harvey_resort) (deceased)
* [Harry Dentin the Bloodhunter](/chars/harry_dentin)
* [Warlock Dylax](/chars/warlock_dylax) (deceased)

## Neutral
* [Carter Blue](/chars/carter_blue)

## Enemies
* [False-God Thumadoin](/chars/thumadoin) (deceased)
* [Adam](/chars/adam) (deceased)
* [The King of Snakes, Zacku](/chars/zacku)
* [The King of the Undead Karn](/chars/karn)
* [The Warlocks of Abaloth](/orgs/abaloth/warlocks)
* [Archmage Mark Lenay](/chars/mark_lenay)
* [Leonin](/chars/leonin)
* [Elden Assassins](/orgs/elden_kingdom/assassins)
* [Prince Damacos the Slave Lord](/chars/slave_lord_damacos) (deceased)
* [Jason Straithe](/chars/jason_straithe) (deceased)

## Relations
#### [Udoven Kingdom](/orgs/udoven_kingdom) (destroyed)
Ravenshade was initially founded in the Udoven Kingdom. The party has done a substantial amount of work for the King and his allies. 

#### [Udoven Republic](/orgs/udoven-republic)
Ravenshade founded the Udoven Republic. The party has done a substantial amount of work in government setup, regulations, and continues to be frequent visitors of the council.

#### [The Leather Strap](/orgs/the_leather_strap)
Ravenshade member Joy Quickfoot was coerced into joining the organization and subsequently the group performed tasks for them.

#### [Elden Kingdom](/orgs/elden_kingdom)
Ravenshade assassinated the original leader of the Elden Kingdom. However, after [Qrow](/chars/qrow) ascended the throne as a puppet for [Queen Mira](/chars/mira_alduin), the group was accepted into the kingdom and regarded as allies. 

#### [Terracloven Kingdom](/orgs/terracloven)
Ravenshade attended the [Conclave of Kings](/events/terracloven/conclave_of_kings) and met [King Haorel](/chars/haorel_willokin). The group performed several good deeds around the town and gained much acclaim in the city for winning [the Victory Pit](/events/terracloven/victory_pit) run by the [Brawler's League](/orgs/terracloven/brawlers_league).

#### [Nightstalkers](/orgs/nightstalkers)
Ravenshade often completes tasks set forth by the Nightstalkers clan due to Joy's alleigance.

#### [Alrich Automatons](/orgs/alrich_automatons)
The company that Tobin's family owns in Terracloven. 

# Quests

## Active
* Take back Udoven Kingdom from General Miller
* Enlist help from Elden Kingdom
* Talk to Hag in Bhever Forest
* Shamen Varak in Kraken Village
* Urdok offered to help deadfire mine (kraken village)
* Karn (help?)
* Urdok asking Orc Chief
* 3 Southern Tribes of Tabaxi (Yorkshire)
* Cradlebush Village
* Milderwood Village (have race)
* Investigate Death Factory (Atticus?)
* Find Keladril
* Akkad? The Castle: Windukes reside, watch elemental chaos of air. Elemental plane of air contains the Orb of Dragonkin. - Tuning Fork in treasure trove, created by The Greatest Thief of all Time. Sonah Ah-Areem. Built a tower in the city of thieves. 

## Completed

## Failed
* Protect [Xolia](/chars/xolia)
# History

## Creation

### Udoven Palace Summoning

### The Attack of the Orcs

### The Royal Ball

## Age of Fire Prophecy

### Red Cube - Sunken Tomb
The red cube was found in a chest belonging to King Karn. Dwyte leaves. Porntip joins.

### Yellow Cube - Elden Kingdom
The yellow cube was found in Adam's belongings in his castle.

### Green Cube - Snake King
The green cube was found in the secret temple of the snake king. Porntip dies.

### Oakwood Village Murder
Brox assassinated the leader of [Oakwood Village](/places/oakwood_village).  

### Mirror Murderer Case
The party solved the [Mirror Murderer](/chars/mirror_murderer) case, but not before Urdok's children are murdered. Both [Rachel Urdok](/chars/rachel_urdok) and [Calvin Urdok](/chars/calvin_urdok) perished.

### The Door of Death
[Tobin](/chars/tobinalrich) joins the party. Party defeated [Kyrasyl](/chars/kyrasyl) the young red dragon. The [Warlocks of Abaloth](/orgs/abaloth/warlocks) escape through before it is closed by the party. Party is offically given the name Ravenshade.

## The Philosopher's Stone

### Urdok Children's Funeral
Wulskin joins. Adam interrupts the funeral and nearly assassinates [Keladryl](/chars/keladryl). Ravenshade defeats him.

### Summoning of the Conclave of Kings
Urdok was summoned to Terracloven in 3 months. Diana leaves.

### Death Factory
Ravenshade ended in the dark woods outside of the factory. They found heavily dismembered and harmed tieflings. The group later liberates the factory after defeating Damacos the Slave Prince. Merida dies at the hands of a death knight.

### Udoven Takeover
The militant leader [General Miller](/chars/general_miller) takes over the Udoven Kingdom in the King's absence. Ravenshade helps contain the tiefling liberation. Fidget and Joy join the party.

### The Underwood Mansion Burning
Brox sets the entire mansion on fire and kills suspicious doppelgangers.

### The Mountain of Secrets Battle Royale
Ravenshade member Wulskin becomes the Grand Champion in the [Battle Royale](/events/mountain_of_secrets/battle_royale) at the [Mountain of Secrets](/places/mountain_of_secrets).

### The Almost-Rebellion
Ravenshade met with [Bildwin Alrich](/chars/bildwin_alrich) and [Silverbeard](/chars/silverbeard). The rebellion stood no chance.

### The Meeting with Seethe
Ravenshade member Brox met with Thumadoin's advisor, Seethe. 

### Brox's Arrest
Brox is arrested on return to Udoven. Brox has a trial. Merida's grave is broken out of. The group escaped.

### The Fontissium Bellator
Brox destroys the machien. Brox is killed by the party, the warlocks plan is revealed. Xolia joins the party.

## Terracloven Pilgrimage

### Kraken's Pass

### Town of Ames
Ott joins.

### The Brambleshire Forest Haunting
Ravenshade kills The Plague Doctor. 

### Bildwin's Ambush
Xoila escapes. 

## Terracloven Conclave

### The Death of Mattis

### The Conclave

### Urdok's Redemption
Rakkon joins.

### Fidget's Second Arrest
Tobin shoots the raven that Wulskin gave Fidget in a moment of extreme annoyance when the animal shits on his face in the Soverenty suite. Fidget shifts into a bird to go bury the body under a tree in the Terracloven Inner District's park. He shift out of form while still in view of the public. Clockwork solders and King's Guard promptly surround him. He is arrested and jailed. Tries to use the opertunity to get a meeting with Leonin. Proceeds to fuck it all up by breaking out; causes Leonin to just get more pissed off at him.

### The Victory Pit Celebration
Ravenshade proves just as mighty out of the arena as in it. Crushes a drinking competition at the ?Blood Hunter's? post victory pit house party.

### Carter Blue's Liberation
Ravenshade accepts the job of defaeting the berserk clockwork warden as a cover to break the subject of a Nightstalker's mission, Carter Blue, out of jail. The tanks of the party spend the whole time focused on the heavy grunt while the healers and ranged attacker were forced to engage the warden in close quarters combat. After a massive struggle (and many fortunate death saves) the party eventually deafeated the foes. Carter Blue pops out of a cell like it was nothing and tells the party he was just trying to see if the Nightstalkers actually gave a shit. Carter casually strolled into the inky darkness of the prision catacombs as the party holds Joy back from trying to punch him in the face. 

The party navigates their way back to the catacomb entrance and as the doors are opened, Carter busts out from the shadows and escapes the prison. The Kings Gaurd lets Ravenshade keep whatever they found, and pays them the reward with the understanding that descresion about the events is needed.

### The Pillars under Terracloven

## The Udoven Liberation

### Zeppelin to Coast City
Tobin dies. Tobin is resurrected by Fidget thanks to herbs from [Leonin's Forest](/places/leonins_forest)

### The Devil in Coast City
Ravenshade kills Yagnolath.

### Coast City Inferno
Ravenshade went to kill goblins to make money for the new boat that is outfitted for the new Udoven revolution. After killing the hobgoblin king, an entire swamp full of gas is exploded by fire. 

### The Pier & Voyage
We spend the day prepping the ship, getting cannons (a special cannon). Collected money (Fidget). 

### Death Factory
Set sail for Death Factory on the way back to Udoven to reclaim it. Defeated a beholder and took tons of weapons built at the factory. 