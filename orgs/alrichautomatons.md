<!-- TITLE: Alrich Automatons -->
<!-- SUBTITLE: A quick summary of Alrich Automatons -->

# Alrich Automatons History
A company specializing in magically powered, mechanized creations founded by a high elf named Aldun Alrich. Aldun has grown his company to be a major force in the city of Terracloven. Aside from selling to many nobles, Alrich Automatons holds contracts to supply the Kings Guard with many varients of their patented Clockwork Soldiers™.