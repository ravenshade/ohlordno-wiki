<!-- TITLE: The Udoven Republic -->
<!-- SUBTITLE: The representative democracy of Udoven -->

# The Udoven Republic
* Founded in 2019 by Tobin, Fidget, and Ravenshade.

# The Council of Udoven
There are 9 major districts divided within Udoven in the first election.
## Members
* Balta (Milderwood)
* Keytal (Yorkshire)
* Vrak the Tortoise
* Kathar (tiefling)
* Clovis
* --- rest is "randomized" ----

## Security
* Kelladrill provides massive warding of the Council chambers

# Guilds
* Worker's Union
* Blacksmith Guild -- Gelladrill
* Mage's Guild -- Kelladrill
* Merchant's Guild
* Licensed Black Market of Udoven -- Xzur (town hall & gave papers)