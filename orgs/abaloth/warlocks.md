<!-- TITLE: Warlocks -->
<!-- SUBTITLE: 10 Warlocks of Abaloth -->

# Living Warlocks
### Lucinia, Warlock of Plague
* Killed by Ravenshade in Terracloven
### Meriel, Warlock of Change
* Killed and fell into the ocean during General Invasion

### Alarak, Warlock of Health
### Rosiree, Warlock of Lust

# Dead Warlocks
### Criminox, Warlock of Blade
* DEAD
* Killed off the clockwork dragon by Terracloven
* Location in Arctic Gate
* Dwight Goodwin
### Warlock of Shadow - 
* DEAD
* in Viktor (Merida)
* Died in Udoven
### Dylax, Warlock of Power
* DEAD
* Previously in brox
* Moved into minotaur summoned by coin by age of fire origin

### Althis, Warlock of Sight
* DEAD
* Seethe, mountain of secrets

### Locosta, Warlock of Forest
* DEAD
* Bertha the Bear during General Invasion

### Warlock of Control
* Located in Udoven
* Locked in Kelladrill
* Went into Fidget upon Kelladrill's death
* Killed by Fidget when he rejected the notion of control

# Known Locations
* Door of Death
* Ocean - Paradine Sea (east coast)
* Mountain of Secrets (Seethe, DEAD)
* Arctic Gate (Blade)

# Great Places of Power
* Door of Death
* Mountain of Secrets
* Death Factory (dead now)
* Terracloven (if something happened)