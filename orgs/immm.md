<!-- TITLE: Institute of Magic and Magical Manipulators -->
<!-- SUBTITLE: A quick summary of IMMM -->

# Institute of Magic and Magical Manipulators
A start up founded by Ravenshade with the backing of King Urdok of Udoven. The institute is devoted to the creation of new magical standards and applications for use across many industries.