<!-- TITLE: Kord's Blessing -->
<!-- SUBTITLE: The Main Ravenshade Ship -->

# Kord's Blessing
## Weapons
* 4x Alrich Arcane Shots (4 barrels - 12s recharge - multiattack - d10 force)
* Spell Slots increase hit and damage (1-3 +3, 4-6 +6, 7-9 ?)

# Crew
* Captain: 
* First Mate: Courtney: human, dark-skin, shaved head, durag
* Navigator: ThatIndividualOverThere:
# Cargo
* Supplies for main journeys

## Weapons
* 50 shields
* 100 longswords
* 120 shortswords
* 70 shortbows
* 2500 arrows
* 1 set Plate Armor
* 10 flails
* 1 pike