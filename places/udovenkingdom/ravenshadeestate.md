<!-- TITLE: Ravenshadeestate -->
<!-- SUBTITLE: A quick summary of Ravenshadeestate -->

# The Ravenshade Estate 
* 3 floors
	* 6 bedrooms
	* 1 living area
	* 1 kitchen
	* 1 workshop
	* 1 training area
* basement
	* training area
* attic
	* locked room with dragonborn statue