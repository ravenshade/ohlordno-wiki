<!-- TITLE: Ravens Nest -->
<!-- SUBTITLE: The Raven's Nest -->

# Raven's Nest
 * 50 floor tower with a penthouse suite
 * Connected by a bridge
 * 30 floor tower (Tobin's)
 * Inner District of Terracloven

## Raven's Nest Floors
 * Floor 1 - Leasing Office
 * Floor 2 - Ultrona Embassy
 * Floor 8 - Kelladrill's room
 * Floor 27 - Wulskin's
 * Floor 40 - Airshop dock

## Raven's Nest Basement
 * Tree Grown inside the basement, hidden by Wall of Stone

# Employment
 * 300 lb magiquartz inside
 * Butler and Footmen (1 yr of pay)
 * Alfonz the Butler