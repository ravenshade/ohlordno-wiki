<!-- TITLE: Home -->
<!-- SUBTITLE: A quick summary of Home -->

# OhLordNo
A wiki for the degenerate group known as [Ravenshade](/orgs/ravenshade.md) to keep track of all their information.
* [Ravenshade Inventory](inventory.md)
* [Collhen Keep & Storage](collhen-keep.md)
* [Ravenshade Quests](/orgs/ravenshade.md#quests)
* [Malestare](/places/malestare.md)
* [Terra](/places/terra.md)
* [Udoven's Republic](/orgs/udoven-republic.md)

# Wiki Organization
There are serveral major categories of the wiki
 * **orgs** - Organizations such as kingdoms, parties, fiefdoms, etc.
 * **chars** - PC and NPCs.
 * **places** - Locations in the world of OhLordNo.
 * **events** - Events that occur in the world. Should include a subdirectory from where they happen, e.g. /events/terracloven/conclave_of_kings.
 *  **items** - Items of significance
 *  **quests** - Major tasks the team needs to perform.

# Templates
There are templates for quickly making pages
* [pc template](/chars/pctemplate) - Player Character Template
* [npc template](/chars/npctemplate) - Non-Player Character Template
* [organization template](/orgs/template) - Main organization template

# Recordings
An inconsistent and fragmented collection of session recordings.

## 2-28-2020: The Finale
<iframe 
		frameborder="0" 
		min-width ="40%"
		width="50%"     
		height="5%"
		src="https://drive.google.com/file/d/1pWa2jO40HF_zCkhGwW5Fe9Y6bxLkaK5v/preview?usp=sharing">    
</iframe>

</br>

## 12-21-2018 Session (part 1)
<iframe 
		frameborder="0" 
		min-width ="40%"
		width="50%"     
		height="5%"
		src="https://drive.google.com/file/d/1VU3EpyH8VQHnxehLZl6mjrP8e8yzqORW/preview">    
</iframe>

</br>

## 1-3-2019 Session
<iframe 
  frameborder="0" 
	min-width ="40%"
  width="50%"     
  height="5%"
  src="https://drive.google.com/file/d/18CuG6Dcs_SAl8YKkpiBe18NzuoiAkmAN/preview">    
</iframe>