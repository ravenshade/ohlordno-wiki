<!-- TITLE: Collhen Keep -->
<!-- SUBTITLE: Ravenshade Estate - Collhen Keep -->

# Collhen Keep

# Layout
* Master Bedroom
* Rooms
* Double Room
* Kitchen
* Foyer 
* Rooms
* Rooms
* Rooms
* Garden
* Basement
* Training area
* Massive deep exit tunnel
* Vault
* Wulskin's Deep Chambers

## Foyer
 * Clockwork Dragon Head mount
 * Massive Key from Wind Plane mount
 * Grandfather Clock

## Garden
* Scrying Holy Pond
* Whiffle-ball Orb on pedestal

## Vault
 * DC 24 Lock, Magic Lock DC 34, Urdok
 * 175 x 175 x 100 ft volume
 * Wall is 40 ft thick
 * Ramp from gate down to bottom, spiral.
 * Wall is stone

## Wulskin's Deep Sex Dungeon
 * Deep ramp 300 x 10 x 10 ft with stone path.
 * Room is 15 x 15 x 15 ft.
# Storage
* 41,925 platinum
* 27,275 gold pieces
* 18,250 silver pieces
* 1000g star sapphire
* 5000g ruby dust
* 4000 gold piece gems
* 2000g gems
* 500g topaz
* 100g jet
* 2 x 1000g blue sapphire
* 1 x 5000g Jacinth
* 1 x star sapphire
* 1 x yellow sapphire
* 1 x fire opal
* 1 x 1000g Black Opal
* 1 x 1000g Opal
* 1 x moss agate
* banded agate (10g)
* Bejewelled Ivory Drinking Horn (7500g)
* Gold Warmask Painted (750g)
* Silk and Velvet Mantleset with Moonstones (2500g)
* manual of clay golem
* 2500g diamond dust


Potions
* Potion of Force Resistance
* Potion of Water Breathing
* 3 x potion radiant
* 3 x potions of supreme healing (10d4 + 20)
* Oil of Etherealness

Items
* Wind Fan
* Arcane Lock Pot + Chain + Bitch Label with Whisper inside
* Deck of Many Things
* breastplate of admantine
* circlet of barbed vision (artifact) - requires neutral attunement
* second celestial spear
* dell bicern staff - staff of healing
* dress of protection +2
* cloak of protection +1
* Ioun Stone of Regeneration
* Robe of Eyes
* Sword of Sharpness

Supplies
* 10 ft cubes of Abyssal Coal
# History
* Given to Ravenshade for the defense 